<?php
$vMob['TIPE']= 'mobil';
$listMob=countRecord('tbl_post', $vMob);
$vMot['TIPE']= 'motor';
$listMot=countRecord('tbl_post', $vMot);
?>
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Mobil <?php echo $listMob['RESULT'][0]['TOTAL'];?></a>

        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Sepeda Motor <?php echo $listMot['RESULT'][0]['TOTAL'];?></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <!-- <a href="damin.php?act=logout" style="color: black; margin-right: 10px;">
            <i class="fas fa-sign-out-alt"></i> Logout
        </a> -->
        <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="assets/img/logo/logo.png?<?php echo rand()?>" class="user-image"
                    style="border-radius:0 !important" alt="User Image">
                <span class="d-none d-md-inline"><?php echo $_SESSION['FULLNAME']?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-light">
                    <img src="assets/img/logo/logo.png?<?php echo rand()?>" class="" alt="User Image">

                    <p>
                        <?php echo $_SESSION['USERNAME']?>
                        <!-- <small>Member since Nov. 2018</small> -->
                    </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                    <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                    <a href="damin.php?act=logout" class="btn btn-default btn-flat float-right">Sign out</a>
                </li>
            </ul>
        </li>
    </ul>

</nav>