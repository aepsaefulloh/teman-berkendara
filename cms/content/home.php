<?php
$vConf['CUSTOM']='STATUS < 99';
$vConf['CATEGORY']='confirmation';
$vSubs['CATEGORY']='subscribe';
$vVer['STATUS']= '1';
$vUV['STATUS']= '0';
$listCount=countRecord('tbl_confirmation',$vConf);
$listCount2=countRecord('tbl_confirmation',$vSubs);
$listVer=countRecord('tbl_confirmation', $vVer);
$listUV=countRecord('tbl_confirmation', $vUV);
?>

<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-primary">
            <div class="inner">
                <h3><?php echo $listCount['RESULT'][0]['TOTAL'];?></h3>

                <p>Jumlah Konfirmasi</p>
            </div>
            <div class="icon">
                <i class="ion ion-android-people"></i>
            </div>
            <!-- <a href="<?php echo CMS_URL.'/index.php?page=data-participant'?>" class="small-box-footer">More info <i
                    class="fas fa-arrow-circle-right"></i></a> -->
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
            <div class="inner">
                <h3><?php echo $listCount2['RESULT'][0]['TOTAL'];?></h3>
                <p>Jumlah Subscription</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-cloud-upload-outline"></i>
            </div>
            <!-- <a href="<?php echo CMS_URL.'/index.php?page=data-post'?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
            <div class="inner">
                <h3><?php echo $listVer['RESULT'][0]['TOTAL'];?></h3>

                <p>Verified</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-checkmark-outline"></i>
            </div>
            <!-- <a href="<?php echo CMS_URL.'/index.php?page=data-participant'?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
        </div>
    </div>
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
            <div class="inner">
                <h3><?php echo $listUV['RESULT'][0]['TOTAL'];?></h3>

                <p>Unverified</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-close-outline"></i>
            </div>
            <!-- <a href="<?php echo CMS_URL.'/index.php?page=data-participant'?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Interaksi User</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Telepon</th>
                            <th>Email</th>
                            <th>Tanggal Konfirmasi</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $vP['LIMIT']=15;
                            $vP['CUSTOM']='STATUS < 99';
                            $vP['ORDER']='TIMESTAMP DESC';
							$list=getRecord('tbl_confirmation',$vP);
							foreach($list['RESULT'] as $list){
                            ?>
                        <tr>
                            <td><?php echo $list['FULLNAME']?></td>
                            <td><?php echo $list['TELP']?></td>
                            <td><?php echo $list['EMAIL']?></td>
                            <td><?php echo $list['TIMESTAMP']?></td>
                            <td> <?php if($list['STATUS']>0){ ?>
                                <a href="" class="badge badge-success">verified</a>
                                <?php }else{ ?>
                                <a href="" class="badge badge-danger">unverified</a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Aktifitas Terakhir</h3>
            </div>
            <div class="card-body table-responsive p-0" style="height: 500px;">
                <table class="table table-head-fixed">
                    <thead>
                        <tr>
                            <th>Role</th>
                            <th>Last Login</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $varLg['LIMIT']=10;
                            $varLg['ORDER']=' LOG_TIMESTAMP DESC';
                            $list=getRecord('tbl_log',$varLg);
                            foreach($list['RESULT'] as $list){
                        ?>
                        <tr>
                            <td><?php echo $list['ACC']?></td>
                            <td><?php tanggal($list['LOG_TIMESTAMP'],'tipe3')?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>