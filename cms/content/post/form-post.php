<?php
$pageseo='post';

$params['ID']=isset($_REQUEST['id'])?$_REQUEST['id']:'';

$act='ADD';
$objDetail=null;
if($params['ID']>0){
	$act='EDIT';	
	$objDetail=getRecord('tbl_'.$pageseo,$params);
	
	$objDetail=$objDetail['RESULT'][0];
	
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <!-- <div class="card-header">
                <h3 class="card-title">Tambah Konten</h3>
            </div> -->
            <!-- /.card-header -->
            <!-- form start -->
            <form class="stdform stdform2" method="post"
                action="<?php echo CMS_URL?>/index.php?page=data-<?php echo $pageseo?>" enctype="multipart/form-data">
                <div class="card-body">
                    <input type='hidden' name='PK-ID' value='<?php echo $objDetail['ID']?>'>
                    <input type='hidden' name='ACT' value='<?php echo $act?>'>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="FULLNAME"
                            value="<?php echo $objDetail['FULLNAME']?>" disabled="disabled">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" value="<?php echo $objDetail['EMAIL']?>" name="EMAIL" disabled="disabled">
                    </div>

                    <div class="form-group">
                        <label>Judul Karya</label>
                        <input type="text" class="form-control" name="TITLE" value="<?php echo $objDetail['TITLE']?>" disabled="disabled">
                    </div>



                    <div class="form-group">
                        <label>Tipe</label>
                        <input type="text" class="form-control" value="<?php echo $objDetail['TIPE']?>" name="TIPE" disabled="disabled">
                    </div>

                    <div class="form-group">
                        <label>Image</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="IMG" disabled="disabled">
                                <label class="custom-file-label">Choose
                                    file</label>
                            </div>
                        </div>
                        <br>
                        <img src="<?php echo ROOT_URL.'/images/'.$pageseo.'/'.$objDetail['IMG']?>" width='300px'>
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <span class="field" style="display:block;">
                            <input type="radio" name="STATUS" value='1'
                                <?php if($objDetail['STATUS']=='1') echo 'checked'?>> Publish
                            <input type="radio" name="STATUS" value='0'
                                <?php if($objDetail['STATUS']=='0') echo 'checked'?>> Unpublish
                        </span>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary" name='submitcontent' value='1'>Save</button>
                    <button class="btn btn-warning" name='submitcontent' value='0' style="color:#fff;">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>