<?php
$pageseo='subscribe';

$submitcontent=isset($_REQUEST['submitcontent'])?$_REQUEST['submitcontent']:'0';
$hal=isset($_REQUEST['hal'])?$_REQUEST['hal']:'1';


//=============IF SUBMIT CONTENT=====================
if($submitcontent=='1'){	
	$objVar=array();
	foreach($_REQUEST as $k=>$v){
		//GET COL VAR
		//echo "raw <i>".strtoupper($k)."=".$v."</i><br>";
		if (in_array(strtoupper($k), $entity['confirmation'])){
			$v=str_replace("'","`",$v);
			$objVar[strtoupper($k)]=$v;
			//echo "<i>".strtoupper($k)."=".$v."</i><br>";
		}
	}

	//IF IMAGE EXIST
	// if($_FILES['IMAGE']['name'] != "") {		
	// 	$allowExt=array('jpg','png','jpeg');
	// 	$ext = pathinfo($_FILES['IMAGE']['name'], PATHINFO_EXTENSION);
	// 	if (in_array($ext, $allowExt)){
	// 		$nfname=md5($_FILES['IMAGE']['name']).'.'.strtolower($ext);
	// 		$target=ROOT_PATH.'/images/confirmation/'.$nfname;			
	// 		move_uploaded_file($_FILES['IMAGE']['tmp_name'],$target);
	// 		$objVar['IMAGE']=$nfname;
	// 	}
	// }
	
	$result=saveRecord('tbl_confirmation',$objVar);
	//echo $result['SQL'];
	
	//----cached-------
	$res=writeCache('tbl_confirmation',$pageseo);
	//echo 'SQL :'.$res['SQL'].'<hr>';
	//----------end cached-------	
	
}

$REC_PERPAGE=20;
$params['LIMIT']=($hal-1)*$REC_PERPAGE.','.$REC_PERPAGE;
$params['ORDER']='ID DESC';
$params['CATEGORY']='subscribe';

$list=getRecord('tbl_confirmation',$params);

?>
<div class="row">
    <div class="col-md-12">
        <div class="card">


            <div class="card-body table-responsive">
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <!-- <th>Hapus</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						foreach($list['RESULT'] as $list){					
						?>
                        <tr>
                            <td><?php echo $list['EMAIL']?></td>
                            <td><?php echo tanggal($list['TIMESTAMP'], 'tipe3'); ?></td>
                            <td><?php if($list['STATUS']>0){ ?>
                                <a href="" class="badge badge-success">Publish</a>
                                <?php }else{ ?>
                                <a href="" class="badge badge-danger">Unpublish </a>
                                <?php } ?>
                            </td>

                            <td>
                                <a href="<?php echo CMS_URL.'/index.php?page=form-'.$pageseo.'&act=edit&id='.$list['ID']?>"
                                    class="btn btn-info btn-sm"><i class="fa fa-user-edit"></i></a>
                            </td>
                            <!-- <td>
                                <a href="<?php echo CMS_URL.'/index.php?page=data-'.$pageseo.'&act=delete&submitcontent=1&pk-id='.$list['ID']?>"
                                    class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></a>
                            </td> -->
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>