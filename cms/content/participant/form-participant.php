<?php
$seo='participant';

$params['EMAIL']=isset($_REQUEST['email'])?$_REQUEST['email']:'';

$act='ADD';
$objDetail=null;
if($params['EMAIL']!=''){
	$act='EDIT';	
	$objDetail=getRecord('tbl_participant',$params);
	$objDetail=$objDetail['RESULT'][0];
}
?>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Participant</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="stdform stdform2" method="post"
                action="<?php echo CMS_URL?>/index.php?page=data-<?php echo $seo?>">
                <input type='hidden' name='PK-EMAIL' value='<?php echo $objDetail['EMAIL']?>'>
                <input type='hidden' name='ACT' value='<?php echo $act?>'>
                <div class="card-body">
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control" placeholder="" name="FULLNAME" id="email2"
                            value="<?php echo $objDetail['FULLNAME']?>">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" placeholder="" name="EMAIL" id="firstname2"
                            value="<?php echo $objDetail['EMAIL']?>">
                    </div>

                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" class="form-control" placeholder="" name="PHONE" id="email2"
                            value="<?php echo $objDetail['PHONE']?>">
                    </div>
                    
                    <div class="form-group">
                        <label>Instagram</label>
                        <input type="text" class="form-control" placeholder="" name="INSTAGRAM" id="email2"
                            value="<?php echo $objDetail['INSTAGRAM']?>">
                    </div>

                    <div class="form-group">
                        <label>Tanggal Registrasi</label>
                        <input type="text" class="form-control" placeholder="" name="REG_DATE" id="email2"
                            value="<?php echo $objDetail['REG_DATE']?>">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <span class="field" style="display:block;">
                            <input type="radio" name="STATUS" value='1'
                                <?php if($objDetail['STATUS']=='1') echo 'checked'?>>
                            Publish
                            <input type="radio" name="STATUS" value='0'
                                <?php if($objDetail['STATUS']=='0') echo 'checked'?>>
                            Unpublish
                        </span>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary" name='submituser' value='1'>Save</button>
                    <button class="btn btn-warning" name='submituser' value='0' style="color:#fff;">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>