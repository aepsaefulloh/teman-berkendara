<?php
$pageseo='participant';

$submitcontent=isset($_REQUEST['submitcontent'])?$_REQUEST['submitcontent']:'0';
$sSearchBanner=isset($_REQUEST['sSearchContent'])?$_REQUEST['sSearchContent']:'';
$hal=isset($_REQUEST['hal'])?$_REQUEST['hal']:'1';


//=============IF SUBMIT CONTENT=====================
if($submitcontent=='1'){	
	$objVar=array();
	foreach($_REQUEST as $k=>$v){
		//GET COL VAR
		//echo "raw <i>".strtoupper($k)."=".$v."</i><br>";
		if (in_array(strtoupper($k), $entity['participant'])){
			$v=str_replace("'","`",$v);
			$objVar[strtoupper($k)]=$v;
			//echo "<i>".strtoupper($k)."=".$v."</i><br>";
		}
	}

	//IF IMAGE EXIST
	if($_FILES['IMG']['name'] != "") {		
		$allowExt=array('jpg','png','jpeg');
		$ext = pathinfo($_FILES['IMG']['name'], PATHINFO_EXTENSION);
		if (in_array($ext, $allowExt)){
			$nfname=md5($_FILES['IMG']['name']).'.'.strtolower($ext);
			$target=ROOT_PATH.'/images/participant/'.$nfname;
			checkFolder();
			move_uploaded_file($_FILES['IMG']['tmp_name'],$target);
			$objVar['IMG']=$nfname;
		}
	}

	 if(($objVar['ACT']=='delete')&&($objVar['PK-EMAIL']!='')){
        $objVar['ACT']='EDIT';
        $objVar['STATUS']=99;
		//echo 'masuk sini';
    }
	$result=saveRecord('tbl_participant',$objVar);
	// echo $result['SQL'];
	
	//----cached-------
	$res=writeCache('tbl_participant',$pageseo);
	//echo 'SQL :'.$res['SQL'].'<hr>';
	//----------end cached-------	
}


$REC_PERPAGE=20;
$params['LIMIT']=($hal-1)*$REC_PERPAGE.','.$REC_PERPAGE;
//$params['sSearchBanner']=$sSearchBanner;
$params['CUSTOM']='STATUS < 99';
$params['ORDER']='REG_DATE DESC';
$list=getRecord('tbl_participant',$params);
//echo $list['SQL'];

?>

<!--  (_)(_)============D  -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <!-- <div class="card-header">
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name='sSearchBanner' class="form-control float-right"
                            placeholder="pencarian.." value='<?php echo $sSearchBanner?>'>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover" id="myTable">
                    <thead class=".t_head">
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Reg Date</th>
                            <th>Status</th>
                            <th>...</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						foreach($list['RESULT'] as $list){					
						?>
                        <tr>
                            <td><?php echo $list['FULLNAME']?><br>

                            </td>
                            <td>
                                <?php echo $list['EMAIL']?>
                            </td>

                            <td><?php echo $list['PHONE']?></td>
                            <td><?php echo $list['REG_DATE']?></td>
                            <td>
                                <?php if($list['STATUS']>0){ ?>
                                <a href="" class="badge badge-success">verified</a>
                                <?php }else{ ?>
                                <a href="" class="badge badge-danger">unverified </a>
                                <?php } ?>
                            </td>
							<td><a href="<?php echo CMS_URL.'/index.php?page=form-'.$pageseo.'&act=edit&email='.$list['EMAIL']?>"
                                class="btn btn-info btn-sm"><i class="fa fa-user-edit"></i></a>
                            <a href="<?php echo CMS_URL.'/index.php?page=data-'.$pageseo.'&act=delete&submitcontent=1&pk-email='.$list['EMAIL']?>"
                                class="btn btn-danger btn-sm"><i class="fa fa-trash-alt"></i></a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0">
                    <a href='<?php echo CMS_URL.'/index.php?page=data-'.$pageseo.'&hal='.($hal-1)?>'>
                        <button class="btn btn-outline-primary"><i class="iconfa-backward"></i> prev</button></a>
                    <button class="btn btn-default ml-2 mr-2"><?php echo $hal?></button>
                    <a href='<?php echo CMS_URL.'/index.php?page=data-'.$pageseo.'&hal='.($hal+1)?>'>
                        <button class="btn btn-outline-primary">next <i class="iconfa-forward"></i></button></a>
                </ul>
            </div>
        </div>
    </div>
</div>