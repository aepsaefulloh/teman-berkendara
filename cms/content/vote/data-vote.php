<?php
$pageseo='vote';

$submitcontent=isset($_REQUEST['submitcontent'])?$_REQUEST['submitcontent']:'0';
$sSearchContent=isset($_REQUEST['sSearchContent'])?$_REQUEST['sSearchContent']:'';
$hal=isset($_REQUEST['hal'])?$_REQUEST['hal']:'1';


//=============IF SUBMIT CONTENT=====================
if($submitcontent=='1'){	
	$objVar=array();
	foreach($_REQUEST as $k=>$v){
		//GET COL VAR
		//echo "raw <i>".strtoupper($k)."=".$v."</i><br>";
		if (in_array(strtoupper($k), $entity['vote'])){
			$v=str_replace("'","`",$v);
			$objVar[strtoupper($k)]=$v;
			//echo "<i>".strtoupper($k)."=".$v."</i><br>";
		}
	}

	//IF IMAGE EXIST
	if($_FILES['IMAGE']['name'] != "") {		
		$allowExt=array('jpg','png','jpeg');
		$ext = pathinfo($_FILES['IMAGE']['name'], PATHINFO_EXTENSION);
		if (in_array($ext, $allowExt)){
			$nfname=md5($_FILES['IMAGE']['name']).'.'.strtolower($ext);
			$target=ROOT_PATH.'/images/post/'.$nfname;			
			move_uploaded_file($_FILES['IMAGE']['tmp_name'],$target);
			$objVar['IMAGE']=$nfname;
		}
	}
	
	$result=saveRecord('tbl_'.$pageseo,$objVar);
	//echo $result['SQL'];
	
	//----cached-------
	$res=writeCache('tbl_'.$pageseo,$pageseo);
	//echo 'SQL :'.$res['SQL'].'<hr>';
	//----------end cached-------	
	
}

$REC_PERPAGE=20;
$params['LIMIT']=($hal-1)*$REC_PERPAGE.','.$REC_PERPAGE;
$params['sSearchContent']=$sSearchContent;
// $params['ORDER'] = 'id desc';
$params['SW'] = '2';
$list=getRecord('tbl_participant',$params);
// echo $list['SQL'];echo '</br>';
$listCT=getVote();
// echo $listCT['SQL'];


?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive p-0">
                <table class="table table-hover" id="myTable">
                    <thead class=".t_head">
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Judul Karya</th>
                            <th>Gambar</th>
                            <th>Jumlah Voting</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                             foreach($listCT['RESULT'] as $listCT){	
                                $vP['ID']=$listCT['ID_IMG'];
                                $res=getRecord('tbl_post',$vP);
                                // echo $res['SQL'];
                                foreach($res['RESULT'] as $res){
                                    $i++;

				
						?>
                        <tr>
                            <td><?php echo $res['FULLNAME']?></td>
                            <td><?php echo $res['EMAIL']?></td>
                            <td><?php echo $res['TITLE']?></td>
                            <td>
                                <a href="<?php echo ROOT_URL.'/images/post/'.$res['IMG'].'?v='.rand()?>?>"
                                    data-toggle="lightbox" data-title="<?php echo $res['TITLE'] ?>">
                                    <img src="<?php echo ROOT_URL.'/images/post/'.$res['IMG'].'?v='.rand()?>?>"
                                        class="img-fluid" style="max-width:80px">
                                </a>
                            </td>
                            <td>
							<?php 
							echo '[ ID : '.$listCT['ID_IMG'].']<br>';
							echo 'Real Count : '.$listCT['TOTAL'].'<br>';
							echo 'Markup :'.$markup[$listCT['ID_IMG']].'<br>';
							echo 'Total : '.($listCT['TOTAL']+$markup[$listCT['ID_IMG']]).'<br>';
							//echo 'Rl : '.$listCT['TOTAL'].'<br>';
							
							//echo '----- markup + '. $markup[$listCT['ID_IMG']].'|'.$listCT['ID_IMG'];
							
							?></td>
                        </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0">
                    <a href='<?php echo CMS_URL.'/index.php?page=data-'.$pageseo.'&hal='.($hal-1)?>'>
                        <button class="btn btn-outline-primary"><i class="iconfa-backward"></i> prev</button></a>
                    <button class="btn btn-default ml-2 mr-2"><?php echo $hal?></button>
                    <a href='<?php echo CMS_URL.'/index.php?page=data-'.$pageseo.'&hal='.($hal+1)?>'>
                        <button class="btn btn-outline-primary">next <i class="iconfa-forward"></i></button></a>
                </ul>
            </div>
        </div>
    </div>

</div>
