<?php
require_once '../config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';


?>
<!DOCTYPE html>
<html>
<head>
	<title>Participant</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
		font-size:12px;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;

	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>

	<?php
	//header("Content-type: application/vnd-ms-excel");
	//header("Content-Disposition: attachment; filename=leads-data.xls");
	
	
	$var['LIMIT']=1000;
	//$var['CUSTOM']=' STATUS < 99';
	//$var['ORDER']=' REG_DATE DESC';
	$list=getRecord('tbl_post',$var);
	//echo $sql;
	?>

	
	<table border="1">
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Telp</th>
			<th>Email</th>
			<th>IG</th>			
			<th>Gambar</th>
			<!-- <th>Kategori</th>			 -->
			<th>Created At</th>
		</tr>
		<?php 
		$i=0;
		foreach($list['RESULT'] as $list){
			// echo $list['IMG'];
			$i++;
			$v['EMAIL']=$list['EMAIL'];
			$ls=getRecord('tbl_participant',$v);
			//echo $ls['SQL'];
			$ls=$ls['RESULT'][0];
			$img='-';
			//if(!empty($ls['RESULT'])){
				$img='https://suzukitetapmelaju.com/digimod/images/post/'.$list['IMG'];	
			//}
		?>
		<tr>
			<td><?php echo $i?></td>
			<td><?php echo $ls['FULLNAME']?></td>
			<td><?php echo $ls['PHONE']?></td>
			<td><?php echo $ls['EMAIL']?></td>			
			<td><?php echo $ls['INSTAGRAM']?></td>	
			<td>
			<a href='<?php echo $img?>' target='_blank'>
			<?php 
			if($img=='-'){
				echo $img;
			}else{ ?>
				<img src='<?php echo $img?>' style='width:100px'>
			<?php }	?>
			</a>
			</td>	
			<!-- <td><?php echo $list['TIPE']?></td>			 -->
			<td><?php echo $list['REG_DATE']?></td>
		</tr>
		<?php } ?>
		
		
	</table>
</body>
</html>
