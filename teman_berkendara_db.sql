-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 08, 2021 at 09:34 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teman_berkendara_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_confirmation`
--

CREATE TABLE `tbl_confirmation` (
  `ID` int(10) NOT NULL,
  `FULLNAME` varchar(100) DEFAULT NULL,
  `TELP` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `INSTAGRAM` varchar(50) DEFAULT NULL,
  `CATEGORY` varchar(50) DEFAULT NULL,
  `TIMESTAMP` datetime DEFAULT NULL,
  `STATUS` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_confirmation`
--

INSERT INTO `tbl_confirmation` (`ID`, `FULLNAME`, `TELP`, `EMAIL`, `INSTAGRAM`, `CATEGORY`, `TIMESTAMP`, `STATUS`) VALUES
(34, 'Aep Saefulloh', '087687367821', 'aepsaefulloh1396@gmail.com', 'aepsaefulloh96', 'confirmation', '2021-10-08 11:22:32', 0),
(35, 'Ujang Candy', '124122131231', '2342343@gmail.com', 'ujankCandy99', 'confirmation', '2021-10-08 11:30:02', 0),
(36, 'jepri kurniawan', '098765456789', 'jefferykuraniawan@gmai.com', 'jejep8778909876', 'confirmation', '2021-10-08 13:48:47', 1),
(37, 'Aep Saefulloh', '1232132', '2342343@gmail.com', 'aepsaefulloh96', 'confirmation', '2021-10-08 13:49:24', 0),
(38, NULL, NULL, 'sesadpopopi@gmail.com', NULL, 'subscribe', '2021-10-08 13:49:44', 1),
(39, NULL, NULL, 'ucup@gmail.com', NULL, 'subscribe', '2021-10-08 15:43:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_group`
--

CREATE TABLE `tbl_group` (
  `ID` int(5) NOT NULL,
  `GROUP_NAME` varchar(100) DEFAULT NULL,
  `STATUS` smallint(1) DEFAULT NULL,
  `ACCESS` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_group`
--

INSERT INTO `tbl_group` (`ID`, `GROUP_NAME`, `STATUS`, `ACCESS`) VALUES
(1, 'Super Admin', 1, 'administrasiuser|confirmation|subscribe|dashboard'),
(2, 'writer', 1, 'confirmation|dashboard');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log`
--

CREATE TABLE `tbl_log` (
  `ID` int(20) NOT NULL,
  `PROC` varchar(50) DEFAULT NULL,
  `ACC` varchar(50) DEFAULT NULL,
  `TBL` varchar(50) DEFAULT NULL,
  `REMARKS` text,
  `LOG_TIMESTAMP` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_log`
--

INSERT INTO `tbl_log` (`ID`, `PROC`, `ACC`, `TBL`, `REMARKS`, `LOG_TIMESTAMP`) VALUES
(1, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:32:11'),
(2, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:41:29'),
(3, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:42:07'),
(4, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:50:09'),
(5, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:52:54'),
(6, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:54:19'),
(7, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:56:34'),
(8, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:57:02'),
(9, 'login', 'superadmin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:57:37'),
(10, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 13:57:45'),
(11, 'login', 'admin', 'tbl_user', '::1 - Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36', '2021-10-08 15:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `ID` int(11) NOT NULL,
  `TITLE` varchar(50) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `TYPE` varchar(10) DEFAULT NULL,
  `POS` varchar(50) DEFAULT NULL,
  `LEVEL` smallint(1) NOT NULL DEFAULT '0',
  `PARENT_ID` smallint(3) DEFAULT NULL,
  `ORDNUM` int(2) DEFAULT '1',
  `STATUS` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`ID`, `TITLE`, `URL`, `TYPE`, `POS`, `LEVEL`, `PARENT_ID`, `ORDNUM`, `STATUS`) VALUES
(27, 'Dashboard', 'dashboard', NULL, 'XMS', 0, 0, 1, 1),
(28, 'Administrasi User', 'administrasiuser', NULL, 'XMS', 0, 0, 2, 1),
(35, 'Participant', 'participant', NULL, 'XMS', 0, 0, 3, 1),
(37, 'Setting', 'setting', NULL, 'XMS', 0, 0, 5, 0),
(48, 'Post', 'post', NULL, 'XMS', 0, 0, 4, 1),
(49, 'motor', 'motor', NULL, 'XMS', 0, 0, 1, 0),
(50, 'Report', 'report', '', 'XMS', 0, 0, 1, 0),
(51, 'Vote', 'vote', NULL, 'XMS', 0, 0, 5, 1),
(52, 'Konfirmasi', 'confirmation', NULL, 'XMS', 0, 0, 3, 1),
(53, 'Subscribe', 'subscribe', NULL, 'XMS', 0, NULL, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_modul`
--

CREATE TABLE `tbl_modul` (
  `ID` int(5) NOT NULL,
  `MODUL` varchar(100) DEFAULT NULL,
  `SEO` varchar(50) DEFAULT NULL,
  `STATUS` smallint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_modul`
--

INSERT INTO `tbl_modul` (`ID`, `MODUL`, `SEO`, `STATUS`) VALUES
(1, 'Administrasi User', 'administrasiuser', 1),
(14, 'Participant', 'participant', 1),
(25, 'setting', 'setting', 0),
(28, 'Menu', 'menu', 1),
(31, 'Post', 'post', 1),
(32, 'Motor', 'motor', 0),
(33, 'Report', 'report', 0),
(34, 'Vote', 'vote', 1),
(35, 'Confirmation', 'confirmation', 1),
(36, 'Subscribe', 'subscribe', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_motor`
--

CREATE TABLE `tbl_motor` (
  `ID` int(5) NOT NULL,
  `TIPE` varchar(50) DEFAULT NULL,
  `JENIS` varchar(50) DEFAULT NULL,
  `IMG` varchar(100) DEFAULT NULL,
  `REMARK` text,
  `STATUS` smallint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_motor`
--

INSERT INTO `tbl_motor` (`ID`, `TIPE`, `JENIS`, `IMG`, `REMARK`, `STATUS`) VALUES
(24, 'Others', '', NULL, '', 1),
(25, 'Big Bikers (>251cc)', '', NULL, '', 1),
(26, 'YOUNGSTAR (FJ110)', '', NULL, '', 1),
(27, 'TRS series', '', NULL, '', 1),
(28, 'THUNDER250 (GS250)', '', NULL, '', 1),
(29, 'THUNDER125 (EN125)', '', NULL, '', 1),
(30, 'SPIN (UY125)', '', NULL, '', 1),
(31, 'SMASH TITAN (FW110)', '', NULL, '', 1),
(32, 'SMASH (FD110/FK110/FV110)', '', NULL, '', 1),
(33, 'SKYWAVE (UW125)', '', NULL, '', 1),
(34, 'SKYDRIVE (UK125)', '', NULL, '', 1),
(35, 'SHOGUN (FD125/FL125)', '', NULL, '', 1),
(36, 'SHOGUN (FD110X)', '', NULL, '', 1),
(37, 'SATRIA (RU120)', '', NULL, '', 1),
(38, 'SATRIA (FU150)', '', NULL, '', 1),
(39, 'RK COOL', '', NULL, '', 1),
(40, 'NEX2 (UX110)', '', NULL, '', 1),
(41, 'NEX (UD110)', '', NULL, '', 1),
(42, 'LET`S (UF110)', '', NULL, '', 1),
(43, 'INAZUMA (GW250)', '', NULL, '', 1),
(44, 'HAYATE (UW125)', '', NULL, '', 1),
(45, 'GSX-S150', '', NULL, '', 1),
(46, 'GSX-R150', '', NULL, '', 1),
(47, 'FXR150', '', NULL, '', 1),
(48, 'FR Series', '', NULL, '', 1),
(49, 'CRYSTAL (RC Series)', '', NULL, '', 1),
(50, 'BURGMAN (UH200)', '', NULL, '', 1),
(51, 'BANDIT (GSX150)', '', NULL, '', 1),
(52, 'ATV', '', NULL, '', 1),
(53, 'ARASHI (FH125)', '', NULL, '', 1),
(54, 'ADDRESS (UK110)', '', NULL, '', 1),
(55, 'A100', '', NULL, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_participant`
--

CREATE TABLE `tbl_participant` (
  `EMAIL` varchar(50) NOT NULL,
  `PASSWORD` varchar(100) DEFAULT NULL,
  `EUP` varchar(100) NOT NULL,
  `FULLNAME` varchar(100) NOT NULL,
  `RANK` varchar(50) DEFAULT NULL,
  `PHONE` varchar(50) DEFAULT NULL,
  `CATEGORY` varchar(10) DEFAULT NULL,
  `REMARK` varchar(100) DEFAULT NULL,
  `COUNTRY` varchar(50) DEFAULT NULL,
  `REG_DATE` datetime DEFAULT NULL,
  `TOKEN` varchar(100) NOT NULL,
  `STATUS` smallint(1) NOT NULL,
  `HIT` int(5) DEFAULT NULL,
  `FACEBOOK` varchar(100) DEFAULT NULL,
  `INSTAGRAM` varchar(100) DEFAULT NULL,
  `CPS` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `ID` int(10) NOT NULL,
  `TITLE` varchar(100) DEFAULT NULL,
  `IMG` varchar(100) DEFAULT NULL,
  `INSTAGRAM` varchar(50) DEFAULT NULL,
  `FACEBOOK` varchar(50) DEFAULT NULL,
  `STATUS` smallint(1) NOT NULL DEFAULT '0',
  `EMAIL` varchar(100) DEFAULT NULL,
  `FULLNAME` varchar(100) DEFAULT NULL,
  `CAPTION` varchar(300) DEFAULT NULL,
  `RANK` varchar(100) DEFAULT NULL,
  `REMARK` varchar(300) DEFAULT NULL,
  `TIPE` varchar(100) DEFAULT NULL,
  `TAHUN` int(4) DEFAULT NULL,
  `CATEGORY` int(2) DEFAULT NULL,
  `HIT` int(5) DEFAULT NULL,
  `POST_TIMESTAMP` datetime DEFAULT NULL,
  `DESCR` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `ID` int(5) NOT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `PASSWD` varchar(200) DEFAULT NULL,
  `EUP` varchar(200) DEFAULT NULL,
  `CPS` varchar(200) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `PHONE` varchar(50) DEFAULT NULL,
  `FULLNAME` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `IMAGE` varchar(100) DEFAULT NULL,
  `ID_GROUP` varchar(50) DEFAULT NULL,
  `LASTLOGIN` datetime DEFAULT NULL,
  `RESET_CODE` varchar(200) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `STATUS` smallint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `USERNAME`, `PASSWD`, `EUP`, `CPS`, `EMAIL`, `PHONE`, `FULLNAME`, `DESCRIPTION`, `IMAGE`, `ID_GROUP`, `LASTLOGIN`, `RESET_CODE`, `CREATED`, `STATUS`) VALUES
(1, 'admin', 'pWmoqKykp5k=', 'lpminqM=|pWmoqKykp5k=', 'p4ssword', 'admin@admin.com', NULL, 'bang juki', '                                                                                                                ', NULL, '1', '2021-10-08 15:48:47', NULL, NULL, 1),
(3, 'superadmin', 'qKmeoaGbp5qaZg==', 'qKqlmqeWmaKeow==|qKmeoaGbp5qaZg==', 'stillfree1', 'aepsaefulloh1396@gmail.com', NULL, 'Aep Saefulloh', 'hola', NULL, '1', '2021-10-08 13:57:37', NULL, NULL, 1),
(4, 'jeffery', 'Zg==', 'n5qbm5qnrg==|Zg==', '1', 'JefferyKurniawan@gmail.com', NULL, 'Jeffery Kurniawan', '                            jefkur', NULL, '2', '2020-06-21 13:17:33', NULL, NULL, 1),
(5, 'popol', 'Zg==', 'paSlpKE=|Zg==', '1', 'popol@gmail.com', NULL, 'kupapopol', '                            ', NULL, '1', '2020-10-08 12:15:33', NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_confirmation`
--
ALTER TABLE `tbl_confirmation`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_group`
--
ALTER TABLE `tbl_group`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_log`
--
ALTER TABLE `tbl_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_modul`
--
ALTER TABLE `tbl_modul`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_motor`
--
ALTER TABLE `tbl_motor`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_participant`
--
ALTER TABLE `tbl_participant`
  ADD PRIMARY KEY (`EMAIL`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_confirmation`
--
ALTER TABLE `tbl_confirmation`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tbl_group`
--
ALTER TABLE `tbl_group`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_log`
--
ALTER TABLE `tbl_log`
  MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `tbl_modul`
--
ALTER TABLE `tbl_modul`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_motor`
--
ALTER TABLE `tbl_motor`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
