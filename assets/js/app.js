var swiper = new Swiper(".mySwiper", {
    slidesPerView: 1,
    loop: true,
    spaceBetween: 10,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        640: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 40,
        },
        1024: {
            slidesPerView: 4,
            spaceBetween: 50,
        },
    },
});

AOS.init();

// Form Confirmation
$(document).ready(function () {
    $('#btn-confirmation').on('click', function () {
        $("#btn-confirmation").attr("disabled", "disabled");
        var fullname = $('#fullname').val();
        var telp = $('#telp').val();
        var email = $('#email').val();
        var instagram = $('#instagram').val();
        if (fullname != "" && telp != "" && email != "" && instagram != "") {
            $.ajax({
                url: "api/confirmation.php",
                type: "POST",
                data: {
                    fullname: fullname,
                    telp: telp,
                    email: email,
                    instagram: instagram,
                },
                cache: false,
                success: function (dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult == 'success') {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Terima kasih atas partisipasi anda',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Mohon periksa kembali pesan nya',
                        })
                    }
                }
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Brads, jangan biarkan isi pesanmu kosong',
            })
        }
    });

    // Form Subscribe
    $('#btn-subscribe').on('click', function () {
        $("#btn-subscribe").attr("disabled", "disabled");
        var email = $('#email_subscribe').val();
        if (email != "") {
            $.ajax({
                url: "api/subscribe.php",
                type: "POST",
                data: {
                    email: email,
                },
                cache: false,
                success: function (dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult == 'success') {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Terima kasih atas partisipasi anda',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Mohon isi form dengan benar',
                        })
                    }
                }
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Brads, jangan biarkan isi pesanmu kosong',
            })
        }
    });
});