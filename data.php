<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Datas</title>
    <!-- Vendor -->
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/vendor/swiperjs/swiper-bundle.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/vendor/aos/aos.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/vendor/jquery.datatable/jquery.dataTables.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/css/styles.css?<?php echo rand()?>">
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/css/responsive.css?<?php echo rand()?>">
</head>

<body>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <table id="example" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nama Lengkap</th>
                                <th>Telepon</th>
                                <th>Email</th>
                                <th>Instagram</th>
                                <th>Kategori</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $var['LIMIT']=99;
                            $list = getRecord('tbl_confirmation', $var);
                            foreach($list['RESULT'] as $list){
                            ?>
                            <tr class="text-center">
                                <td><?php echo $list['FULLNAME']?></td>
                                <td><?php echo $list['TELP']?></td>
                                <td><?php echo $list['EMAIL']?></td>
                                <td><?php echo $list['INSTAGRAM']?></td>
                                <td><?php echo $list['CATEGORY']?></td>
                                <td><?php echo tanggal($list['TIMESTAMP'], 'tipe3'); ?></td>
                                <td><?php echo $list['STATUS']?></td>
                                <td><button class="btn btn-info">Update</button></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>


    <script src="<?php echo ROOT_URL?>/assets/vendor/jquery/jquery-3.6.0.min.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/jquery/jquery.validate.min.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/swiperjs/swiper-bundle.min.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/aos/aos.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/sweetalert2/sweetalert2@11.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/jquery.datatable/jquery.dataTables.min.js"></script>
    <!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
    <script src="<?php echo ROOT_URL?>/assets/js/app.js?<?php echo rand()?>"></script>

    <script>
    $(document).ready(function() {
        var table = $('#example').DataTable({});
    });
    </script>
</body>

</html>