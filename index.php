<?php
require_once 'config.php';
require_once ROOT_PATH.'/lib/dao_utility.php';
require_once ROOT_PATH.'/lib/mysqlDao.php';
// require_once ROOT_PATH.'/lib/json_utility.php';
// $secret_key = "6LdxG8YZAAAAAOGhnq6TLPEMIAMsJRWUvlC9cKx0";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>Teman Berkendara - Suzuki Indonesia</title>
    <meta name="description" content="Sudah 50 Tahun Suzuki hadir sebagai Teman Berkendara bagi Suzuki Family. Kali ini kami
                        menghadirkan sebuah event dengan tajuk yang sama. Berharap Suzuki Family tetap menjadi bagian
                        dari perjalanan kami di tahun-tahun selanjutnya. Selamat mengikuti dan semoga beruntung! ">
    <meta name="keywords"
        content="Suzuki Indonesia Teman Berkendara, Event Suzuki Indonesia, Suzuki Event, Suzuki Teman Berkendara">
    <meta name="author" content="matarasa.co.id">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo ROOT_URL?>/assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
        href="<?php echo ROOT_URL?>/assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo ROOT_URL?>/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo ROOT_URL?>/assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo ROOT_URL?>/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo ROOT_URL?>/assets/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor -->
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/vendor/swiperjs/swiper-bundle.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/vendor/aos/aos.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/css/styles.css?<?php echo rand()?>">
    <link rel="stylesheet" href="<?php echo ROOT_URL?>/assets/css/responsive.css?<?php echo rand()?>">
</head>

<body>
    <nav class="navbar navbar-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="<?php echo ROOT_URL?>/assets/img/logo/logo-color.png" alt="" width="80"
                    class="d-inline-block align-text-top">
            </a>
            <div class="m-0">
                <a class="navbar-brand" href="#">
                    <img src="<?php echo ROOT_URL?>/assets/img/icon/facebook-dark.png" alt="" width="20"
                        class="d-inline-block align-text-top">
                </a>
                <a class="navbar-brand" href="#">
                    <img src="<?php echo ROOT_URL?>/assets/img/icon/instagram-dark.png" alt="" width="20"
                        class="d-inline-block align-text-top">
                </a>
                <a class="navbar-brand" href="#">
                    <img src="<?php echo ROOT_URL?>/assets/img/icon/youtube-dark.png" alt="" width="20"
                        class="d-inline-block align-text-top">
                </a>
            </div>

        </div>
    </nav>
    <section class="section-hero hero-desktop py-5">
        <div class="container">
            <div class="row py-lg-5">
                <div class="hero-images">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/1.png" width="100" class="image-1" alt="avatar-1">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/2.png" width="120" class="image-2" alt="avatar-2">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/3.png" width="150" class="image-3" alt="avatar-3">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/4.png" width="150" class="image-4" alt="avatar-4">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/1.png" width="120" class="image-5" alt="avatar-5">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/1.png" width="100" class="image-6" alt="avatar-6">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/5.png" width="120" class="image-7" alt="avatar-7">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/3.png" width="150" class="image-8" alt="avatar-8">

                </div>
                <div class="hero-text">
                    <img src="<?php echo ROOT_URL?>/assets/img/logo/hero.png?<?php echo rand()?>" width="70%"
                        class="img-fluid" data-aos="fade-right" alt="Hero Teman Berkendara">
                    <!-- <h1 class="">TEMAN</h1>
                    <h1 class="">BERKENDARA</h1> -->
                    <!-- <p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Posuere suspendisse auctor
                        integer molestie<br>dictum enim nunc, et. </p> -->
                </div>
            </div>
        </div>
    </section>
    <section class="section-hero hero-mobile py-5">
        <div class="container">
            <div class="row py-lg-5">
                <div class="hero-images">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/1.png" width="150" class="image-1" alt="avatar-1">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/2.png" width="120" class="image-2" alt="avatar-2">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/3.png" width="150" class="image-3" alt="avatar-3">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/4.png" width="150" class="image-4" alt="avatar-4">
                    <img src="<?php echo ROOT_URL?>/assets/img/avatar/1.png" width="120" class="image-5" alt="avatar-5">
                </div>
                <div class="hero-text">
                    <img src="<?php echo ROOT_URL?>/assets/img/logo/hero.png?<?php echo rand()?>" data-aos="fade-right"
                        data-aos-duration="1000" class="img-fluid" alt="Hero Teman Berkendara">
                    <!-- <h1 class="">TEMAN</h1>
                    <h1 class="">BERKENDARA</h1> -->
                    <!-- <p class="mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Posuere suspendisse auctor
                        integer molestie<br>dictum enim nunc, et. </p> -->
                </div>
            </div>
        </div>
    </section>
    <section class="section-navbar pb-5">
        <div class="container">
            <hr>
            <div class="row">
                <ul class="nav justify-content-evenly">
                    <li class="nav-item">
                        <a class="nav-link" href="#about">Tentang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tnc">Syarat</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#winner">Hadiah</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#confirmation">Konfirmasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#gallery">Galeri</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="section-about py-2 py-md-5" id="about">
        <div class="container">
            <img src="<?php echo ROOT_URL?>/assets/img/pattern_plus_blue.png" class="pattern-image" alt="vector">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="<?php echo ROOT_URL?>/assets/img/about.png" class="img-fluid image-about" alt="about">
                </div>
                <div class="col-md-6">
                    <h3>Teman Berkendara</h3>
                    <p>Sudah 50 Tahun Suzuki hadir sebagai Teman Berkendara bagi Suzuki Family. Kali ini kami
                        menghadirkan sebuah event dengan tajuk yang sama. Berharap Suzuki Family tetap menjadi bagian
                        dari perjalanan kami di tahun-tahun selanjutnya. Selamat mengikuti dan semoga beruntung! </p>
                </div>
            </div>
        </div>
    </section>
    <section class="section-tnc py-5" id="tnc">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4>Syarat & Ketentuan</h4>
                </div>
                <div class="col-12 mt-5">
                    <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-home-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home"
                                aria-selected="true">REGULASI</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-profile-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile"
                                aria-selected="false">DISCLAIMER</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact"
                                aria-selected="false">MEKANISME</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane text-white fade " id="pills-home" role="tabpanel"
                            aria-labelledby="pills-home-tab">
                            <ul class="">
                                <li>Kontes Teman Berkendara hanya dapat diikuti oleh warga negara Indonesia yang
                                    berdomisili di Indonesia, berusia 17 tahun ke atas.</li>
                                <li>Kontes Teman Berkendara dapat diikuti oleh siapa saja.</li>
                                <li>Kontes Teman Berkendara tidak dikenai biaya (Gratis).</li>
                                <li>PPeserta harus mendaftar di situs web Suzuki (Microsite) untuk berpartisipasi dalam
                                    Kontes Teman Berkendara./li>
                                <li>Follow akun Instagram <a href="https://www.instagram.com/suzuki_id/"
                                        target="_blank">Suzuki
                                        Indonesia</a> dan
                                    <a href="https://www.instagram.com/suzukiindonesiamotor/" target="_blank">Suzuki
                                        Motor
                                        Indonesia.</a>
                                </li>
                                <li>Like Facebook <a href="https://www.facebook.com/suzukiindonesia"
                                        target="_blank">Suzuki
                                        Indonesia</a> dan <a href="https://www.facebook.com/SuzukiMotorcyclesID"
                                        target="_blank">Suzuki
                                        Motor
                                        Indonesia.</a></li>
                                <li>Kontes Teman Berkendara akan berlangsung dari tanggal 15 Oktober - 21 November 2021.
                                </li>
                                <li>Juri akan menilai berdasarkan kreativitas para peserta dan keputusan juri tidak
                                    dapat diganggu gugat. </li>
                                <li>Untuk 200 orang tercepat yang mengunggah Kontes Teman Berkendara, akan mendapatkan
                                    hadiah menarik dari Suzuki Indonesia.</li>
                                <li>Juri akan memilih 3 pemenang terbaik dan akan ditampilkan di akun Instagram serta
                                    Facebook Suzuki Indonesia pada 31 November 2021 setelah Kontes Teman Berkendara
                                    ditutup.</li>
                            </ul>
                        </div>
                        <div class="tab-pane text-white fade show active" id="pills-profile" role="tabpanel"
                            aria-labelledby="pills-profile-tab">
                            <ul class="">
                                <li>Karya yang dikirim ke Kontes Teman Berkendara tidak menampilkan merek lain selain
                                    Suzuki, elemen negatif, kekerasan, pornografi, politik, SARA, dan apa pun yang
                                    melanggar hak kekayaan intelektual.</li>
                                <li>Setiap karya yang akan dikirim ke Kontes Teman Berkendara adalah karya orisinal,
                                    bukan hasil menjiplak karya orang lain/pihak tertentu dan tidak pernah
                                    diikutsertakan dalam kompetisi atau publikasi media.</li>
                                <li>Semua karya yang dikirim peserta dalam teman berkendara akan menjadi milik PT Suzuki
                                    Indomobil Sales dan dapat digunakan sepenuhnya untuk kepentingan promosi atau
                                    kegiatan lainnya.
                                </li>
                                <li>PT Suzuki Indomobil Sales memiliki hak untuk mendiskualifikasi peserta yang tidak
                                    sesuai dengan ketentuan yang berlaku dan peserta yang terbukti melakukan kecurangan
                                    yang membuat persaingan menjadi tidak adil. </li>
                                <li>Setiap peserta boleh mengirimkan lebih dari satu karya untuk mengikuti Kontes Teman
                                    Berkendara.</li>
                                <li>Jika ditemukan indikasi kecurangan oleh peserta atau peserta melanggar ketentuan
                                    yang sudah ditetapkan, maka secara otomatis karya yang sudah dikirim akan segera
                                    didiskualifikasi dari Kontes Teman Berkendara.</li>
                                <li>Pajak hadiah ditanggung oleh pemenang.</li>
                            </ul>
                        </div>
                        <div class="tab-pane text-white fade" id="pills-contact" role="tabpanel"
                            aria-labelledby="pills-contact-tab">
                            <ul class="">
                                <li>Setiap peserta wajib mengunggah karya yang akan diikutsertakan pada kontes Teman
                                    Berkendara di akun Instagram masing-masing.</li>
                                <li>Pastikan akun Instagram peserta tidak dalam kondisi private account alias public
                                    account.</li>
                                <li>Sertakan hashtag #TemanBerkendara di postingan tersebut. Jangan lupa menandai (tag)
                                    akun Instagram Suzuki Indonesia <a href="https://www.instagram.com/suzuki_id/"
                                        target="_blank">(@suzuki_id)</a> dan Suzuki Motor
                                    Indonesia
                                    <a href="https://www.instagram.com/suzukiindonesiamotor/"
                                        target="_blank">(@suzukiindonesiamotor),</a> sebutkan pula momen berharga kalian
                                    bareng
                                    Suzuki yang
                                    tidak terlupakan.
                                </li>
                                <li>Peserta Kontes Teman Berkendara wajib mengisi kolom di microsite dengan Nama, Email,
                                    Nomor Handphone, dan Username Instagram.
                                </li>
                                <li>Pihak PT Suzuki Indomobil Sales akan melakukan kurasi terhadap karya yang memenuhi
                                    kriteria dan layak untuk masuk ke dalam galeri Microsite Teman Berkendara.</li>
                                <li>Pihak PT Suzuki Indomobil Sales akan memilih 3 karya terbaik berdasarkan
                                    orisinalitas, tema, dan kreativitas.</li>
                                <li>Keputusan PT Suzuki Indomobil Sales tidak bisa diganggu gugat.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <section class="section-winner py-5" id="winner">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">HADIAH</h2>
                </div>
                <div class="col-12">
                    <div class="the-winner">
                        <div class="grid-winner">
                            <div class="column-winner">
                                <h5>Juara 1</h5>
                                <img src="<?php echo ROOT_URL?>/assets/img/nex.png" width="100"
                                    class="img-fluid enlarge py-2" alt="">
                                <h5>Nex II</h5>
                            </div>
                            <div class="column-winner">
                                <h5>Juara 2</h5>
                                <img src="<?php echo ROOT_URL?>/assets/img/duit.png" width="100"
                                    class="img-fluid enlarge py-2" alt="">
                                <h5>3 Juta Rupiah</h5>
                            </div>
                            <div class="column-winner">
                                <h5>Juara 3</h5>
                                <img src="<?php echo ROOT_URL?>/assets/img/duit.png" width="100"
                                    class="img-fluid enlarge py-2" alt="">
                                <h5>2 Juta Rupiah</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-form" id="confirmation">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="box-form" id="box-form">
                        <h2>Konfirmasi</h2>

                        <form class="row g-3 mt-5" method="POST" id="form-confirmation">
                            <input type='hidden' name='ACT' value='ADD'>
                            <input type='hidden' name='CATEGORY' id="category" value='confirmation'>
                            <div class="col-md-6">
                                <label class="form-label">Nama</label>
                                <input type="text" name="FULLNAME" id="fullname" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">No. HP</label>
                                <input type="number" name="TELP" id="telp" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Email</label>
                                <input type="email" name="EMAIL" id="email" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Akun IG</label>
                                <input type="text" name="INSTAGRAM" id="instagram" class="form-control" required>
                            </div>

                            <!-- <div class="col-12">
                                <div class="g-recaptcha d-flex justify-content-center"
                                    data-sitekey="6LcZoNUZAAAAAEmycTT-Yc9vprs4gh63V2WfgdgM">
                                </div>
                            </div> -->
                            <div class="col-12 text-center">
                                <button type="submit" id="btn-confirmation"
                                    class="btn btn-red rounded-0 px-5 py-2">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-gallery py-5" id="gallery">
        <div class="container">
            <div class="row py-lg-5">
                <div class="col-12">
                    <div class="gallery-heading">
                        <h1>GALERI</h1>
                    </div>
                    <div class="swiper mySwiper mt-5">
                        <div class="swiper-wrapper text-center">
                            <?php
                                // $var['LIMIT']=12;
                                // $list = getRecord('tbl_confirmation', $var);
                                // foreach($list['RESULT'] as $list){
                                    for ($i=0; $i < 6; $i++) { 
                                        # code...
                                    
                            ?>
                            <div class="swiper-slide">
                                <img src="<?php echo ROOT_URL?>/assets/img/gallery.png" alt="">
                            </div>
                            <?php } ?>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="section-footer py-5">
        <div class="container">
            <div class="row">
                <div class="grid-footer">
                    <div class="footer-column">
                        <p>Present By</p>
                        <img src="<?php echo ROOT_URL?>/assets/img/logo/logo-white.png" width="80" alt="footer-logo">
                    </div>
                    <div class="footer-column">
                        <p>Automobile</p>
                        <div class="footer-sosmed">
                            <a href="https://www.facebook.com/suzukiindonesia" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/facebook-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                            <a href="https://www.instagram.com/suzuki_id/" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/instagram-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                            <a href="https://twitter.com/SuzukiIndonesia" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/twitter-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                            <a href="https://www.youtube.com/user/SuzukiID" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/youtube-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                            <a href="https://www.tiktok.com/@suzuki_id" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/tiktok-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                        </div>
                    </div>
                    <div class="footer-column">
                        <p>Motocycle</p>
                        <div class="footer-sosmed">
                            <a href="https://www.facebook.com/SuzukiMotorcyclesID" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/facebook-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                            <a href="https://www.instagram.com/suzukiindonesiamotor/" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/instagram-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                            <a href="https://twitter.com/suzukimotorid" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/twitter-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                            <a href="https://www.youtube.com/c/SuzukiMotorcyclesIndonesia" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/youtube-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                            <a href="https://www.tiktok.com/@suzukiindonesiamotor?" target="_blank">
                                <img src="<?php echo ROOT_URL?>/assets/img/icon/tiktok-white.png" width="20"
                                    alt="footer-logo">
                            </a>
                        </div>
                    </div>
                    <div class="footer-column">
                        <p>Subscribe</p>
                        <form method="POST" id="form-subscribe">
                            <input type='hidden' name='ACT' value='ADD'>
                            <input type="email" class="form-control" name="EMAIL" id="email_subscribe"
                                placeholder="Email">
                            <button class="" type="submit" id="btn-subscribe">
                                <img src="<?php echo ROOT_URL?>/assets/img/vector-send.png" width="20">
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-copyright text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="mt-2">Copyright © 2021 Suzuki. All Rights Reserved.</p>
                </div>
                <div class="col">

                </div>
            </div>
        </div>
    </section>
    <script src="<?php echo ROOT_URL?>/assets/vendor/jquery/jquery-3.6.0.min.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/jquery/jquery.validate.min.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/swiperjs/swiper-bundle.min.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/aos/aos.js"></script>
    <script src="<?php echo ROOT_URL?>/assets/vendor/sweetalert2/sweetalert2@11.js"></script>
    <!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
    <script src="<?php echo ROOT_URL?>/assets/js/app.js?<?php echo rand()?>"></script>


    <script>

    </script>

</body>

</html>